package fi.vamk.tka;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    // create a method helloWeekday that will get a day string (like "19/03/2020")as
    // argument and return Hello Thursday World!
    // the weekday will change based on when the app is runned
}
